#!/bin/bash

echo "Creating solution structure with projects in a Clean Architecture fashion.."

# Solution and gitignore
dotnet new sln
dotnet new gitignore

# Layers
dotnet new classlib --name Chat.Domain --output src/Chat.Domain
dotnet new classlib --name Chat.Application --output src/Chat.Application
dotnet new web --name Chat --output src/Chat

# Dependencies between layers
dotnet add src/Chat.Application reference src/Chat.Domain
dotnet add src/Chat reference src/Chat.Application

# Test projects
dotnet new xunit --name Chat.Domain.UnitTests --output test/Chat.Domain.UnitTests
dotnet new xunit --name Chat.Application.UnitTests --output test/Chat.Application.UnitTests
dotnet new xunit --name Chat.FunctionalTests --output test/Chat.FunctionalTests

# Dependencies for test projects
dotnet add test/Chat.Domain.UnitTests reference src/Chat.Domain
dotnet add test/Chat.Application.UnitTests reference src/Chat.Application
dotnet add test/Chat.FunctionalTests reference src/Chat

# Packages for unit tests projects
dotnet add test/Chat.Domain.UnitTests package FluentAssertions
dotnet add test/Chat.Application.UnitTests package FluentAssertions
dotnet add test/Chat.Domain.UnitTests package Moq
dotnet add test/Chat.Application.UnitTests package Moq

# Packages for functional test project
dotnet add test/Chat.FunctionalTests package SpecFlow.xUnit
dotnet add test/Chat.FunctionalTests package SpecFlow.Plus.LivingDocPlugin
dotnet add test/Chat.FunctionalTests package FluentAssertions
dotnet add test/Chat.FunctionalTests package Microsoft.AspNetCore.Mvc.Testing

# Add projects to solution
dotnet sln add src/Chat.Domain
dotnet sln add src/Chat.Application
dotnet sln add src/Chat
dotnet sln add test/Chat.Domain.UnitTests
dotnet sln add test/Chat.Application.UnitTests
dotnet sln add test/Chat.FunctionalTests
